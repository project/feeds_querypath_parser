<?php

/**
 * @file
 *
 * Provides the class for FeedsQueryPathParser.
 */

class FeedsQueryPathParser extends FeedsParser {

  /**
   * Implementation of FeedsParser::parse().
   */
  public function parse(FeedsImportBatch $batch, FeedsSource $source) {
    $batch->setTitle(trim(qp($batch->getRaw(), 'title')->text()));

    $this->source_config = $source->getConfigFor($this);
    $this->rawXML = array_keys(array_filter($this->source_config['rawXML']));

    foreach (qp($batch->getRaw(), $this->source_config['context']) as $child) {
      $parsed_item = array();
      foreach ($this->source_config['sources'] as $source => $query) {
        $parsed_item[$source] = $this->parseSourceElement($child, $query, $source);
      }
      $batch->addItem($parsed_item);
    }
  }

  protected function parseSourceElement($item, $query, $source) {
    $attr = $this->source_config['attrs'][$source];

    if ($query != '') {
      $item = qp($item, $query);
    }

    if ($attr != '') {
      return $item->attr($attr);
    }

    if (in_array($source, $this->rawXML)) {
      return $item->html();
    }
    else {
      return $item->text();
    }
  }

  /**
   * Source form.
   */
  public function sourceForm($source_config) {
    $form = array();
    $form['#weight'] = -10;

    $mappings_ = feeds_importer($this->id)->processor->config['mappings'];
    $uniques = $mappings = array();

    foreach ($mappings_ as $mapping) {
      if (strpos($mapping['source'], 'querypathparser:') === 0) {
        $mappings[$mapping['source']] = $mapping['target'];
        if ($mapping['unique']) {
          $uniques[] = $mapping['target'];
        }
      }
    }

    if (empty($mappings)) {
      $form['error_message']['#value'] = 'FeedsQueryPathParser: No mappings were defined.';
      return $form;
    }

    $form['context'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Context'),
      '#required'      => TRUE,
      '#description'   => t('This is the base query, all other queries will run in this context.'),
      '#default_value' => isset($source_config['context']) ? $source_config['context'] : '',
    );

    $form['sources'] = array(
      '#type' => 'fieldset',
    );
    $form['attrs'] = array(
      '#title' => t('Attributes'),
      '#type' => 'fieldset',
      '#description' => t('If you want an attribute returned for a field, type its name here.')
    );
    $items = array(
      format_plural(count($uniques),
        t('Field <strong>!column</strong> is mandatory and considered unique: only one item per !column value will be created.',
          array('!column' => implode(', ', $uniques))),
        t('Fields <strong>!columns</strong> are mandatory and values in these columns are considered unique: only one entry per value in one of these columns will be created.',
          array('!columns' => implode(', ', $uniques)))),
    );

    $form['sources']['help']['#value'] = '<div class="help">' . theme('item_list', $items) . '</div>';

    foreach ($mappings as $source => $target) {
      $form['sources'][$source] = array(
        '#type'          => 'textfield',
        '#title'         => $target,
        '#description'   => t('The CSS selector for this field.'),
        '#default_value' => isset($source_config['sources'][$source]) ? $source_config['sources'][$source] : '',
      );
      $form['attrs'][$source] = array(
        '#type'          => 'textfield',
        '#title'         => $target,
        '#description'   => t('The attribute to return.'),
        '#default_value' => isset($source_config['attrs'][$source]) ? $source_config['attrs'][$source] : '',
      );
    }

    $form['rawXML'] = array(
      '#type'          => 'checkboxes',
      '#title'         => t('Select the queries you would like to return raw XML or HTML'),
      '#options'       => $mappings,
      '#default_value' => isset($source_config['rawXML']) ? $source_config['rawXML'] : array(),
    );

    return $form;
  }

  /**
  * Override parent::getMappingSources().
  */
  public function getMappingSources() {
    return array(
      'querypathparser:0' => array(
        'name' => t('QueryPath Expression'),
        'description' => t('Allows you to configure an CSS expression that will populate this field.'),
      ),
    ) + parent::getMappingSources();
  }

  /**
   * Define defaults.
   */
  public function sourceDefaults() {
    return array(
      'context' => '',
      'sources' => array(),
      'attrs'   => array(),
      'rawXML'  => array(),
    );
  }

  /**
   * Override parent::sourceFormValidate().
   *
   * Simply trims all values from the form. That way when testing them
   * later we can be sure that there aren't any strings with spaces in them.
   *
   * @param &$values
   *   The values from the form to validate, passed by reference.
   */
  public function sourceFormValidate(&$values) {
    $values['context'] = trim($values['context']);
    foreach ($values['sources'] as &$query) {
      $query = trim($query);
    }
    foreach ($values['attrs'] as &$attr) {
      $attr = trim($attr);
    }
  }
}

/**
 * Implementation of hook_form_feeds_ui_mapping_form_alter().
 *
 * This is an interesting bit of work. Each source name has to be unique,
 * but we have no idea how many to create with getMappingSources() because we
 * don't know how many targets there are going to be.
 *
 * Solution is to keep track in the form how many have been added.
 */
function feeds_querypath_parser_form_feeds_ui_mapping_form_alter($form, &$form_state) {
  $newest_xpath_mapping = array();
  foreach ($form['#mappings'] as $mapping) {
    if (strpos($mapping['source'], 'querypathparser:') === 0) {
      $newest_xpath_mapping = $mapping;
    }
  }
  if (!empty($mapping)) {
    list($a, $count) = explode(':', $newest_xpath_mapping['source']);
    $default_source = $a . ':' . '0';
    $label = $form['source']['#options'][$default_source];
    unset($form['source']['#options'][$default_source]);
    $form['source']['#options'][$a . ':' . ++$count] = $label;
  }
  return $form;
}
